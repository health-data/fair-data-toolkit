# FAIR Data Toolkit

## Getting started

- https://faircookbook.elixir-europe.org/
- https://howtofair.dk/how-to-fair/
- https://www.go-fair.org/fair-principles/fairification-process/
- https://fairsharing.org/
- https://frictionlessdata.io/

### Data maturity
- https://www.dataorchard.org.uk/what-is-data-maturity

### Software maturity
- https://software.ac.uk/

### Document initial datasets
- https://ddialliance.org/training/getting-started/data-catalog
- https://huggingface.co/docs/datasets/dataset_card
- https://ddialliance.org/training/getting-started-new-content/create-a-codebook
- https://fairsharing.org/search?fairsharingRegistry=Standard
- https://rdmkit.elixir-europe.org/metadata_management

### Guidelines
- https://impact-data.bsc.es/files/202302.IMPaCT-Data.Requirements_Recommendations.pdf
